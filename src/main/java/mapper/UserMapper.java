package mapper;

import com.yh.javabean.User;

import java.util.ArrayList;
import java.util.HashMap;

public interface UserMapper {

    ArrayList<User> selectAll();
    User selectById(int id);
    ArrayList<User> selectByNameAndId(int id,String name);
    ArrayList<User>  selectByUser1(User user);
    ArrayList<User>  selectByUser2(User user);
    ArrayList<User> selectByIds(int[] ids);
    ArrayList<User> selectByList(ArrayList<Integer> list);
    ArrayList<User> selectByUserList(ArrayList<User> list);
    ArrayList<User> selectByMap(HashMap<String,String> map);
    int insertUser(User user);

    int updateSalById(int id,double sal);

    int deleteById(int id);

}
