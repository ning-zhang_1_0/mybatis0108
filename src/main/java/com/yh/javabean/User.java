package com.yh.javabean;

import java.util.Date;


    public class User {
        private int id;
        private String name;
        private double sal;
        private Date birthday;

        public User() {
        }
        public User(int id) {
            this.id = id;
        }
        public User(int id, String name, Double sal, Date birthday) {
            this.id = id;
            this.name = name;
            this.sal = sal;
            this.birthday = birthday;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getSal() {
            return sal;
        }

        public void setSal(double sal) {
            this.sal = sal;
        }

        public Date getBirthday() {
            return birthday;
        }

        public void setBirthday(Date birthday) {
            this.birthday = birthday;
        }

        @Override
        public String toString() {
            return "User{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", sal=" + sal +
                    ", birthday=" + birthday +
                    '}';
        }
    }

