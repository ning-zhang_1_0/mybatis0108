package com.yh.util;

import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class updateSalByIdTest {
    public static void main(String[] args) {
        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();
        SqlSession session = factory.openSession(true);
        UserMapper mapper = session.getMapper(UserMapper.class);

        int i = mapper.updateSalById(3, 100);
        System.out.println(i);

        session.close();
    }
}
