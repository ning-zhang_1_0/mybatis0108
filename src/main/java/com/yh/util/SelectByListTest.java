package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;
import java.util.Collections;

public class SelectByListTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        UserMapper mapper = session.getMapper(UserMapper.class);

        ArrayList<Integer> list=new ArrayList<>();
        Collections.addAll(list,1,2,3);
        ArrayList<User> users = mapper.selectByList(list);
        for (User user : users) {
            System.out.println(user);
        }

        session.close();
    }
}
