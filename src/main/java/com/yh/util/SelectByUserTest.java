package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;

public class SelectByUserTest {
    public static void main(String[] args) {
        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();
        SqlSession session = factory.openSession(true);
        UserMapper mapper = session.getMapper(UserMapper.class);

        User u=new User();
        u.setId(1);
        u.setName("鲁智深");

        ArrayList<User> users1 = mapper.selectByUser1(u);
        System.out.println(users1);

        ArrayList<User> users2 = mapper.selectByUser2(u);
        System.out.println(users2);
        session.close();
    }
}
