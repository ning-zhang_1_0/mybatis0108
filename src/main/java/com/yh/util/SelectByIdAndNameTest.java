package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;

public class SelectByIdAndNameTest {
    public static void main(String[] args) {
        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();
        SqlSession session = factory.openSession(true);
        UserMapper mapper = session.getMapper(UserMapper.class);

        ArrayList<User> user = mapper.selectByNameAndId(1, "鲁智深");

        System.out.println(user);
        session.close();
    }
}
