package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;

public class SelectByIdsTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        UserMapper mapper = session.getMapper(UserMapper.class);

        int [] ids={1,2,3};
        ArrayList<User> users = mapper.selectByIds(ids);
        for (User user : users) {
            System.out.println(user);
        }

        session.close();
    }
}
