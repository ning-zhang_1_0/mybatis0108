package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.Date;

public class insertUserTest {
    public static void main(String[] args) {
        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();
        SqlSession session = factory.openSession(true);
        UserMapper mapper = session.getMapper(UserMapper.class);

        User user=new User(3,"张三",500.0,new Date());
        int i = mapper.insertUser(user);
        System.out.println(i);

        session.close();
    }
}
