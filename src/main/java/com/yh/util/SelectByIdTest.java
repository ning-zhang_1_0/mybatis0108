package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class SelectByIdTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        UserMapper mapper = session.getMapper(UserMapper.class);

        User user = mapper.selectById(1);
        System.out.println("id:"+user);

        session.close();
    }
}
