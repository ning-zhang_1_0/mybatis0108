package com.yh.util;

import com.yh.javabean.User;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

public class MyBatisTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        List<User> users = session.selectList("selectAll");

        for(User u:users){
            System.out.println(u);
        }
        //5、关闭资源
        session.close();
    }
}
