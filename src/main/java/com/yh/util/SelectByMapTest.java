package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectByMapTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        UserMapper mapper = session.getMapper(UserMapper.class);

        HashMap<String ,String> map=new HashMap<>();
        map.put("mid","1");
        map.put("mname","鲁智深");

        ArrayList<User> users = mapper.selectByMap(map);
        for (User user : users) {
            System.out.println(user);
        }

        session.close();
    }
}
