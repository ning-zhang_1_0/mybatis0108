package com.yh.util;

import com.yh.javabean.User;
import mapper.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.ArrayList;
import java.util.Collections;

public class SelectByUserListTest {
    public static void main(String[] args) {

        SqlSessionFactory factory = MyBatisUtil.getSqlSessionFactory();

        SqlSession session = factory.openSession(true);

        UserMapper mapper = session.getMapper(UserMapper.class);

        ArrayList<User> list=new ArrayList<>();
        Collections.addAll(list,new User(1),new User(2),new User(3));
        ArrayList<User> users = mapper.selectByUserList(list);
        for (User user : users) {
            System.out.println(user);
        }

        session.close();
    }
}
